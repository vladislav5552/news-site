import React from 'react'
import './footer.css'


const   Footer = () =>

 
 <div class="container">
    <div class="row">
         <div class="footer-border">
             <div class="col-md-3">
                 <div class="footer-date-createsite">
                    18+ © Украина.ру, 2018.
                 </div>
             </div>
             <div class="col-md-9">
                 <div class="footer-description">
                     Все результаты интеллектуальной деятельности, размещенные на сайте https://ukraina.ru/, охраняются в соответствии с правом Российской Федерации или иным применимым правом. .<br>
                     Использование произведений, правообладателем которых являются третьи лица, размещенных на сайте https://ukraina.ru/, запрещено без получения разрешения правообладателя.<br>
                     По всем вопросам, возникающим при использовании сайта, в том числе связанным с нарушением прав использования результатов интеллектуальной деятельности просим обращаться editors@ukraina.ru<br>
                     Мнение владельца сайта может не совпадать с точкой зрения авторов публикаций
                 </div>
             </div>
        </div>
    </div>
</div> 

export default Footer